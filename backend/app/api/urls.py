from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from app.api.views import (
    ClientViewSet,
    FittingViewSet,
    HoseSizeViewSet,
    HoseTypeViewSet,
    HoseViewSet,
    MuftaViewSet,
)

router = routers.DefaultRouter()
router.register(r"mufta", MuftaViewSet, basename="Mufta")

router.register(r"hose/type", HoseTypeViewSet, basename="HoseType")
router.register(r"hose/size", HoseSizeViewSet, basename="HoseSize")
router.register(r"hose", HoseViewSet, basename="Hose")

router.register(r"fitting", FittingViewSet, basename="Fitting")

router.register(r"client", ClientViewSet, basename="Client")

urlpatterns = [
    url(r"^", include(router.urls)),
]
