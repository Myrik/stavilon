from django.http import Http404
from rest_framework import viewsets

from app.fitting.models import Fitting, Mufta
from app.fitting.serializer import FittingSerializer, MuftaSerializer
from app.hose.models import Hose, HoseSize, HoseType
from app.hose.serializer import HoseSerializer, HoseSizeSerializer, HoseTypeSerializer
from app.logistic.models import Client
from app.logistic.serializer import ClientSerializer


class FittingViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = FittingSerializer

    def get_queryset(self):
        fitting_id = self.kwargs.get("pk")

        if fitting_id is not None:
            return Fitting.objects.filter(id=fitting_id)

        fittings = Fitting.objects.all()

        du_id = self.request.query_params.get("du_id")
        if du_id is not None:
            fittings = fittings.filter(du_id=int(du_id))

        du = self.request.query_params.get("du")
        if du is not None:
            fittings = fittings.filter(du__dn=int(du))

        angle = self.request.query_params.get("angle")
        if angle is not None:
            fittings = fittings.filter(angle=int(angle))

        return fittings


class HoseTypeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = HoseType.objects.all()
    serializer_class = HoseTypeSerializer


class HoseSizeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = HoseSize.objects.all()
    serializer_class = HoseSizeSerializer


class HoseViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = HoseSerializer

    def get_queryset(self):
        hose_id = self.kwargs.get("pk")

        if hose_id:
            return Hose.objects.filter(id=hose_id)

        hoses = Hose.objects.all()
        if "size" in self.request.query_params:
            size = int(self.request.query_params["size"])
            hoses = hoses.filter(size=size)
        if "type" in self.request.query_params:
            t = int(self.request.query_params["type"])
            hoses = hoses.filter(type=t)

        return hoses


class MuftaViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = MuftaSerializer

    @staticmethod
    def get_hose_type(pk):
        try:
            hose = HoseType.objects.prefetch_related("clean_Mufta", "unclean_mufta").get(
                id=pk
            )
        except Hose.DoesNotExist:
            raise Http404
        return hose

    def get_queryset(self):
        hose_type_id = self.request.query_params.get("hose_type")

        if hose_type_id is not None:
            hose_type = self.get_hose_type(hose_type_id)

            clean_mufta = hose_type.clean_mufta.all()
            unclean_mufta = hose_type.unclean_mufta.all()

            rez = set(unclean_mufta)
            rez.update(clean_mufta)
            return rez

        mufta_id = self.kwargs.get("pk")

        if mufta_id:
            return Mufta.objects.filter(id=mufta_id)

        return Mufta.objects.all()


class ClientViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Client.objects.order_by("coef")
    serializer_class = ClientSerializer
