# Generated by Django 2.2.1 on 2019-06-19 12:03

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('logistic', '0003_client'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='client',
            options={'verbose_name': 'Клиет', 'verbose_name_plural': 'Клиенты'},
        ),
    ]
