from django.apps import AppConfig


class LogisticsConfig(AppConfig):
    name = "app.logistic"
    verbose_name = "Логистика"
