from django.contrib import admin

from app.logistic.models import Client, Location, Order


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ("name", "address")


@admin.register(Order)
class OrderzAdmin(admin.ModelAdmin):
    list_display = ("hose", "fitting_start", "fitting_end", "mufta_start", "mufta_end")


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ("name", "coef")
