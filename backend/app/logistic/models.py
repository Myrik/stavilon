from django.db import models


class Location(models.Model):
    class City:
        CHOICES = (
            (0, "Неизвестно"),
            (1, "Минск"),
            (2, "Витебск"),
        )

    name = models.CharField(max_length=300, default="")
    address = models.CharField(max_length=300, default="", blank=True)
    city = models.IntegerField(choices=City.CHOICES, default=1)

    class Meta:
        verbose_name = "Локация"
        verbose_name_plural = "Локации"

    def __str__(self):
        return self.name


class Order(models.Model):
    hose = models.ForeignKey("hose.Hose", on_delete=models.CASCADE)

    fitting_start = models.ForeignKey(
        "fitting.Fitting", on_delete=models.CASCADE, related_name="fitting_start"
    )
    fitting_end = models.ForeignKey(
        "fitting.Fitting", on_delete=models.CASCADE, related_name="fitting_end"
    )

    mufta_start = models.ForeignKey(
        "fitting.Mufta", on_delete=models.CASCADE, related_name="mufta_start"
    )
    mufta_end = models.ForeignKey(
        "fitting.Mufta", on_delete=models.CASCADE, related_name="mufta_end"
    )

    count = models.IntegerField("Кол-во", default=1)

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"


class Client(models.Model):
    name = models.CharField(max_length=30)
    coef = models.FloatField("Коэфицент", default=1)

    class Meta:
        verbose_name = "Клиет"
        verbose_name_plural = "Клиенты"
