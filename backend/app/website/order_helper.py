from app.fitting.models import Fitting
from app.hose.models import Hose
from app.logistic.models import Client


class OrderHelper:
    hose: Hose
    hose_len: int
    left_fitting: Fitting
    right_fitting: Fitting
    client: Client

    def __init__(self, params: dict):
        if params.get("hose"):
            self.hose = Hose.objects.get(id=params["hose"])
        else:
            hoses = Hose.objects.filter(
                type=params["hose_type"], size=params["hose_size"]
            )
            if hoses.exists():
                raise Exception("Рукав не найден")
            self.hose = hoses.first()

        self.hose_len = int(params["hose_len"] or 0)
        if 0 >= self.hose_len > 100_000:
            raise Exception("Недопустимая длина рукава")

        if params["left_fitting"] != params["right_fitting"]:
            self.left_fitting = Fitting.objects.get(id=params["left_fitting"])
            self.right_fitting = Fitting.objects.get(id=params["right_fitting"])
        else:
            self.left_fitting = self.right_fitting = Fitting.objects.get(
                id=params["left_fitting"]
            )

        # if params.get('left_mufta'):
        #     left_mufta = Mufta.objects.get(id=params['left_mufta'])
        # else:
        #     left_mufta = hose.mufta
        #
        # if params.get('right_mufta'):
        #     right_mufta = Mufta.objects.get(id=params['right_mufta'])
        # else:
        #     right_mufta = hose.mufta

        self.client = Client.objects.get(id=params["client"])

    @property
    def fitting_text(self):
        if self.left_fitting == self.right_fitting:
            return str(self.left_fitting)
        return f"{self.left_fitting}-{self.right_fitting}"

    @property
    def hose_price(self):
        return self.hose.price * (self.hose_len / 1000)

    @property
    def fitting_price(self):
        return self.left_fitting.price + self.right_fitting.price

    @staticmethod
    def len_coef(l):
        # 1.2 -> 1
        c_len = 5000
        c = 1 + (c_len - l) / c_len * 0.2
        return max(c, 1)

    @property
    def price(self):
        return self.hose_price + self.hose.type.price + self.fitting_price

    @property
    def cost(self):
        hose = self.len_coef(self.hose_len) * self.hose_price + self.hose.type.price
        return (hose + self.fitting_price) * self.client.coef

    def __str__(self):
        return f"{self.hose}x{self.hose_len},{self.fitting_text}"
