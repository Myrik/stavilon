from django.conf.urls import url

from app.website.views import HoseTree, OrderApi, OrderView

urlpatterns = [
    url(r"^$", OrderView.as_view()),
    url(r"^order$", OrderApi.as_view()),
    url(r"^tree$", HoseTree.as_view()),
]
