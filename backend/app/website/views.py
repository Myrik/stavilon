from django.core.handlers.wsgi import WSGIRequest
from django.http import JsonResponse
from django.views import View
from django.views.generic import TemplateView

from app.hose.models import Hose
from app.website.order_helper import OrderHelper


class OrderView(TemplateView):
    template_name = "main.html"

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(items=[])
        return self.render_to_response(context)


class HoseTree(View):
    @staticmethod
    def get(request):
        ret = {}
        hoses = Hose.objects.prefetch_related("size", "type")
        for hose in hoses:
            r = ret.get(hose.size.id, {})
            d = r.get(hose.type.id, [])

            d.append({"name": hose.name or None, "id": hose.id})

            r[hose.type.id] = d
            ret[hose.size.id] = r
        return JsonResponse(ret)


class OrderApi(View):
    def post(self, request: WSGIRequest):
        params = request.POST.dict()
        print(params)
        try:
            order = OrderHelper(params)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=500)

        return JsonResponse(
            {
                "text": str(order),
                "price": round(order.price, 2),
                "cost": round(order.cost, 2),
                "nds": round(order.cost * 1.2, 2),
            }
        )
