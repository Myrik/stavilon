from rest_framework import serializers

from app.hose.models import Hose, HoseSize, HoseType


class HoseSizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = HoseSize
        fields = ("id", "dn", "size", "inch")


class HoseTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = HoseType
        fields = ("id", "ch", "op", "text")


class HoseSerializer(serializers.ModelSerializer):
    type = serializers.PrimaryKeyRelatedField(read_only=True)
    size = serializers.PrimaryKeyRelatedField(read_only=True)
    name = serializers.CharField()
    text = serializers.SerializerMethodField()

    class Meta:
        model = Hose
        fields = ("id", "name", "type", "size", "text")

    def get_text(self, obj):
        return str(obj)
