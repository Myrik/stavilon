from django.db import models


class HoseType(models.Model):
    ch = models.CharField("Тип", max_length=3, default="SN")
    op = models.IntegerField("Оплетки", default=1)
    description = models.CharField("Описание", max_length=300, blank=True, default="")
    text = models.CharField("Текст", max_length=10, blank=True, default="")

    price = models.FloatField("Цена работы", default=0)

    def __str__(self):
        if self.op:
            return f"{self.op}{self.ch}"
        return str(self.ch)

    def __repr__(self):
        return str(self)

    class Meta:
        verbose_name = "Тип РВД"
        verbose_name_plural = "Типы РВД"
        unique_together = ("ch", "op")
        ordering = ("op", "ch")

    def save(self, *args, **kwargs):
        self.text = str(self)
        super().save(*args, **kwargs)


class HoseSize(models.Model):
    dn = models.IntegerField("Номинальный диаметр", default=0)
    size = models.IntegerField("Обозначение размера", default=0)
    inch = models.CharField("Дюймы", max_length=30, default="")

    class Meta:
        verbose_name = "Размер РВД"
        verbose_name_plural = "Размеры РВД"
        ordering = ("dn",)
        unique_together = ("dn",)

    def __str__(self):
        return str(self.dn).zfill(2)


class Hose(models.Model):
    name = models.CharField("Название", max_length=300, default="", blank=True)

    size = models.ForeignKey(HoseSize, on_delete=models.CASCADE)
    type = models.ForeignKey(HoseType, on_delete=models.CASCADE)

    mufta = models.ForeignKey(
        "fitting.Mufta", on_delete=models.SET_NULL, null=True, blank=True
    )

    dia_in = models.IntegerField("Внутренний диаметр", default=0, blank=True)
    dia_out = models.IntegerField("Наружный диаметр", default=0, blank=True)
    bar = models.IntegerField("Разрывное давление", default=0, blank=True)
    rad = models.IntegerField("Радиус изгиба", default=0, blank=True)

    price = models.FloatField("Цена за м", default=0)

    class Meta:
        verbose_name = "Рукава"
        verbose_name_plural = "Рукав"
        unique_together = ("name", "type", "size")

    def __str__(self):
        if self.name:
            return f"{self.name} {self.size} {self.type}"
        return f"{self.size} {self.type}"
