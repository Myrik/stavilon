from django.apps import AppConfig


class HoseConfig(AppConfig):
    name = "app.hose"
    verbose_name = "РВД"
