from django.contrib import admin
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter

from app.hose.models import Hose, HoseSize, HoseType


@admin.register(Hose)
class HoseAdmin(admin.ModelAdmin):
    list_display = ("text", "size", "type")
    list_filter = (
        ("size", RelatedDropdownFilter),
        ("type", RelatedDropdownFilter),
    )

    def text(self, obj):
        return str(obj)


@admin.register(HoseType)
class HoseTypeAdmin(admin.ModelAdmin):
    list_display = ("text",)

    def text(self, obj):
        return str(obj)


@admin.register(HoseSize)
class HoseSizeAdmin(admin.ModelAdmin):
    list_display = ("dn", "size", "inch")
