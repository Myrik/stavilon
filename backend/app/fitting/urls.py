from django.conf.urls import url

from app.fitting.views import AddFit

urlpatterns = [
    url("^add$", AddFit.as_view()),
]
