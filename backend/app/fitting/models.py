import re

from django.db import models


class Thread(models.Model):
    class Types:
        metric = 1
        inch = 2
        uni_inch = 3
        ring = 4
        conical = 5

        CHOICES = (
            (0, "Неизвестно"),
            (metric, "Метрическая"),
            (inch, "Дюймовая"),
            (uni_inch, "Унифицированная Дюймовая"),
            (ring, "Под кольцо"),
            (conical, "Коническая"),
        )

        CHECK = (
            (metric, r"[MМ]\f[xх]\f"),
            (inch, r'G\f[\/]\f"?|G\f"'),
            (uni_inch, r'(\d+ )?\f[\/]\f"?|\f"'),
            (ring, r"\d+"),
        )

    text = models.CharField("Текст", max_length=128, default="")
    type = models.IntegerField("Тип", choices=Types.CHOICES, default=0)

    def __str__(self):
        return f"{self.text}"

    def __repr__(self):
        return str(self)

    class Meta:
        verbose_name = "Резьба"
        verbose_name_plural = "Резьбы"
        unique_together = ("text",)
        ordering = ("id",)

    @classmethod
    def from_str(cls, text: str):
        text = (
            text.strip()
            .upper()
            .replace("М", "M")
            .replace("Х", "X")
            .replace("X", "x")
            .replace(",", ".")
            .replace(r"\\", "/")
        )

        f = r"\d+(?:\.d+)?"
        thread_variants = [r"\f", r"\f/\f" r"M(\f)(X\f)?", r"[GD]\f/\f"]
        for i in thread_variants:
            if re.findall(i.replace(r"\f", f), text):
                break
        else:
            raise Exception(f"Unknown thread format: {text}")

        item = cls.objects.get(text=text)
        return item

    def save(self, *args, **kwargs):
        self.text = (
            self.text.strip()
            .upper()
            .replace("М", "M")
            .replace("Х", "X")
            .replace("X", "x")
            .replace(",", ".")
            .replace(r"\\", "/")
        )

        f = r"\d+(?:\.d+)?"
        thread_variants = [r"\f", r"\f/\f" r"M(\f)(X\f)?", r"[GD]\f/\f"]
        for i in thread_variants:
            if re.findall(i.replace(r"\f", f), self.text):
                break
        else:
            raise Exception(f"Unknown thread format: {self.text}")

        if self.type == 0:
            for i in self.Types.CHECK:
                exp = "^" + i[1].replace(r"\f", r"\d+([\.,]\d+)?") + "$"
                if re.findall(exp, self.text):
                    self.type = i[0]
        super().save(*args, **kwargs)


class Standart(models.Model):
    class Type:
        CHOICES = (
            (0, "Незвестно"),
            (1, "Резьба"),
            (2, "Ключ"),
            (3, "Труба"),
            (4, "Для пустотельного винта"),
            (5, "Фланец"),
        )

    name = models.CharField("Имя", max_length=10)
    alt_name = models.CharField(
        "Альтернативное имя", max_length=10, default="", blank=True
    )
    type = models.IntegerField("Тип", choices=Type.CHOICES, default=0)
    description = models.CharField("Описание", max_length=10, blank=True, default="")

    def __str__(self):
        return f"{self.name}({self.alt_name})"

    class Meta:
        verbose_name = "Стандарт"
        verbose_name_plural = "Стандарты"
        unique_together = ("name", "alt_name")
        ordering = ("name", "alt_name")


class Fitting(models.Model):
    class Angle:
        CHOICES = (
            (90, "90°"),
            (45, "45°"),
            (30, "30°"),
            (0, "0°"),
        )

    name = models.CharField("Производитель", max_length=128, default="", blank=True)

    du = models.ForeignKey(
        "hose.HoseSize", verbose_name="ДУ шланга", on_delete=models.CASCADE
    )
    standart = models.ForeignKey(
        "Standart", verbose_name="Стандарт", on_delete=models.CASCADE
    )
    thread = models.ForeignKey(
        "Thread",
        verbose_name="Резьба",
        on_delete=models.CASCADE,
        null=True,
        default=None,
        blank=True,
    )

    angle = models.IntegerField("Наклон", choices=Angle.CHOICES, default=0)

    text = models.CharField("Наименование", max_length=128, blank=True, default="")

    price = models.FloatField("Цена", default=0)

    class Meta:
        verbose_name = "Фитинг"
        verbose_name_plural = "Фитинги"
        unique_together = ("name", "du", "standart", "thread", "angle")
        ordering = ("standart", "du", "thread")

    def save(self, *args, **kwargs):
        self.text = str(self)
        super().save(*args, **kwargs)

    def __str__(self):
        data = (self.standart.name, self.angle, self.thread, self.name)
        return " ".join([str(i) for i in data if i])

    @classmethod
    def from_str(cls, text: str):
        text = (
            text.strip()
            .upper()
            .replace("М", "M")
            .replace("Х", "X")
            .replace(",", ".")
            .replace(r"\\", "/")
        )

        items = re.match(
            r'(?P<dia>\d{1,2}) *(?P<standart>[A-Z]+)(?P<angle> *\d{2})? +(?P<thread>[\d\\\/,."DMXG]+)',
            text,
        )
        if not items:
            return f"Parse error: {text}"
        else:
            items = items.groupdict()
        angle = int(items["angle"] or 0)

        if angle not in [i[0] for i in cls.Angle.CHOICES]:
            return f"Angle error: {text}"

        standart, created = Standart.objects.get_or_create(name=items["standart"])

        thread = Thread.from_str(items["thread"])
        item, created = cls.objects.get_or_create(
            du=items["dia"], standart=standart, thread=thread, angle=angle
        )
        return item


class Mufta(models.Model):
    name = models.CharField(max_length=300)

    if_clean = models.ManyToManyField(
        "hose.HoseType", related_name="clean_mufta", blank=True, symmetrical=False
    )
    if_unclean = models.ManyToManyField(
        "hose.HoseType", related_name="unclean_mufta", blank=True, symmetrical=False
    )

    price = models.FloatField("Цена", default=0)

    class Meta:
        verbose_name = "Муфта"
        verbose_name_plural = "Муфты"

    def __str__(self):
        return self.name
