from django.apps import AppConfig


class FittingConfig(AppConfig):
    name = "app.fitting"
    verbose_name = "Фитинги"
