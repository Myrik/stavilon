# Generated by Django 2.2.1 on 2019-05-06 13:44

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fitting', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fitting',
            name='angle',
            field=models.IntegerField(choices=[(90, '90°'), (45, '45°'), (30, '30°'), (0, '0°')], default=0,
                                      verbose_name='Наклон'),
        ),
    ]
