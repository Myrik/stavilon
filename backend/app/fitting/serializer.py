from rest_framework import serializers

from app.fitting.models import Fitting, Mufta, Standart


class MuftaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mufta
        fields = ("id", "name", "if_clean", "if_unclean")


class StandartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Standart
        fields = ("id", "name", "alt_name", "type", "description")


class FittingSerializer(serializers.ModelSerializer):
    standart = StandartSerializer(read_only=True)
    thread = serializers.SlugRelatedField(read_only=True, slug_field="text")

    class Meta:
        model = Fitting
        fields = ("id", "name", "text", "du", "standart", "thread", "angle")
