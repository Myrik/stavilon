from django.contrib import admin
from django_admin_listfilter_dropdown.filters import (
    ChoiceDropdownFilter,
    RelatedDropdownFilter,
)

from app.fitting.models import Fitting, Mufta, Standart, Thread


class StandartListFilter(admin.SimpleListFilter):
    title = "Стандарт"

    parameter_name = "standart"

    def lookups(self, request, model_admin):
        standarts = set(
            Fitting.objects.all().values_list("standart__id", "standart__name")
        )
        return standarts

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(standart_id=self.value())


@admin.register(Fitting)
class FittingAdmin(admin.ModelAdmin):
    list_display = ("text", "du", "standart", "thread", "angle")
    autocomplete_fields = ("thread", "standart")
    search_fields = ("text",)
    readonly_fields = ("text",)
    list_filter = (
        ("standart", RelatedDropdownFilter),
        ("du", RelatedDropdownFilter),
        ("thread", RelatedDropdownFilter),
        ("angle", ChoiceDropdownFilter),
    )


@admin.register(Mufta)
class MuftaAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Standart)
class StandartAdmin(admin.ModelAdmin):
    list_display = ("name", "alt_name", "type")
    search_fields = ("name", "alt_name")


@admin.register(Thread)
class ThreadAdmin(admin.ModelAdmin):
    list_display = ("text", "type")
    list_filter = ("type",)
    search_fields = ("text",)
