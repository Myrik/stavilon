from django.shortcuts import redirect
from django.views.generic import TemplateView

from app.fitting.models import Fitting


class AddFit(TemplateView):
    template_name = "AddFit.html"

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("user/")

        text = request.POST["text"].split("\n")

        items = [Fitting.from_str(i) for i in text if i.strip()]

        context = self.get_context_data(items=items)
        return self.render_to_response(context)
