#!/bin/bash
./manage.py dumpdata --indent 2 fitting.thread --format json_ascii > fixtures/fitting.thread.json
./manage.py dumpdata --indent 2 fitting.standart --format json_ascii > fixtures/fitting.standart.json
./manage.py dumpdata --indent 2 fitting.fitting --format json_ascii > fixtures/fitting.fitting.json
./manage.py dumpdata --indent 2 fitting.mufta --format json_ascii > fixtures/fitting.mufta.json

./manage.py dumpdata --indent 2 hose.hosetype --format json_ascii > fixtures/hose.hosetype.json
./manage.py dumpdata --indent 2 hose.hosesize --format json_ascii > fixtures/hose.hosesize.json
./manage.py dumpdata --indent 2 hose.hose --format json_ascii > fixtures/hose.hose.json

./manage.py dumpdata --indent 2 logistic.location --format json_ascii > fixtures/logistic.location.json
./manage.py dumpdata --indent 2 logistic.client --format json_ascii > fixtures/logistic.client.json

./manage.py dumpdata --indent 2 auth.user --format json_ascii > fixtures/auth.user.json
