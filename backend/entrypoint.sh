#!/bin/sh
python3 manage.py collectstatic --noinput

while ! nc -z database 5432; do sleep 1; done;

python3 manage.py migrate
python3 manage.py loaddata fixtures/*
python3 manage.py runserver 0.0.0.0:8001
